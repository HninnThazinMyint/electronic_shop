@extends('master')

@section('content')

    <div class="container">
        <p><a href="{{ url('/shop') }}">Shop</a> / {{ $product->name }}</p>
        <h1>{{ $product->name }}</h1>

        <hr>

        <div class="row">
            <div class="col-md-6">
              <div class="panel-body">
                <?php
                   $images = array();
                   $images[] = explode("|", $product['image']);
                 ?>
                 <img src="{{ asset('img/' . $images[0][0]) }}" alt="product" class="img-thumbnail" style="width: 150px; height: 150px;">
                 <br><br>
              </div>
              <div class="footer">
                <?php
                   for($i=1; $i<count($images[0]); $i++){
                 ?>
                  <img src="{{ asset('img/' . $images[0][$i]) }}" alt="product" class="img-thumbnail" style="width: 120px; height: 120px;">
                  <?php
                     }
                   ?>
              </div>
              <br><br>

          </div>

            <div class="col-md-6">
                <h3 class="text-primary">${{ $product->price }}</h3>

                <form action="{{ url('/cart') }}" method="POST" class="side-by-side">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{ $product->id }}">
                    <input type="hidden" name="name" value="{{ $product->name }}">
                    <input type="hidden" name="price" value="{{ $product->price }}">
                    <input type="submit" class="btn btn-warning btn-lg" value="Add to Cart">
                </form>

                <br><br>

                <i class="text-primary">{{ $product->description }}</i>
            </div> <!-- end col-md -->
        </div> <!-- end row -->


    </div> <!-- end container -->

@endsection
