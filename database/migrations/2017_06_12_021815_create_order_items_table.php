<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('product_id');
            $table->Integer('order_id');
            $table->Integer('qty');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function product()
    {
        return $this->hasOne('App\Product','id');
    }
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
