@extends('master')

@section('content')

  @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered table-hover" >
          <thead>
              <tr class="bg-info text-primary" style="background: #eee;font-size: 20px;">
                  <th>Customer Information</th>
                  <th colspan="2">Items(Qty)</th>
              </tr>
          </thead>
          <tbody>
              @foreach ($order_list as $item)
              <tr class="bg-info">
                <td>
                  <ul>
                    <li>{{ $item['name'] }}</li>
                    <li>{{ $item['email'] }}</li>
                    <li>{{ $item['address'] }}</li>
                    <li><b><i>Order Date::&nbsp;</i></b>{{ $item['created_at'] }}</li>
                  </ul>
                </td>
                <td>
                  <ul>
                    @foreach($item->orderitem as $orderitems)
                      <li>
                        <a href="{{url('shop/'.$orderitems['product']['slug'])}}">
                          <?php echo $orderitems['product']['name']; ?>   </a>
                          (<?php     echo $orderitems['qty']; ?>)

                      </li>

                    @endforeach
                  </ul>
                </td>
                <td>
                  <a href="{{url('emptyorder/'.$item['id'])}}" class="btn btn-danger">
                          Delete   </a>
                </td>
              </tr>


              @endforeach
          </tbody>
        </table>
      </div>
  </div>
</div>

@endsection
