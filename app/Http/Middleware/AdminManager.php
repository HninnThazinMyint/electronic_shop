<?php

namespace App\Http\Middleware;

use Closure;

class AdminManager
{
	public function handle($request, Closure $next)
    {
     if(\Auth::check()){

    	if ($request->user()->role == 1)		    
				{
					return $next($request);	
				}	
			}
		
		return redirect('shop');
 }
 
}
