@extends('master')

@section('content')

    <div class="container">
        <p><a href="{{ url('shop') }}">Home</a> / Cart</p>
        <h1>Your Cart</h1>

        <hr>

        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif

        @if(Auth::guest())
            @if (sizeof(Cart::content()) > 0)        
        <table class="table">
                <thead>
                    <tr>
                        <th class="table-image"></th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                </thead>           
                <tbody>
                    @foreach (Cart::content() as $item)
                    <?php
                       $images = array();
                       $images[] = explode("|", $item->model['image']);
                    ?>
                     <tr>
                        <td class="table-image">
                          <a href="{{ url('shop', [$item->model['slug']]) }}">
                            <img src="{{ asset('img/' . $images[0][0]) }}" alt="product" class="img-responsive cart-image">
                          </a>
                        </td>
                        <td><a href="{{ url('shop', [$item->model['slug']]) }}">{{ $item->name }}</a></td>
                        <td>
                            <select class="quantity" data-id="{{ $item->rowId }}">
                                <option {{ $item->qty == 1 ? 'selected' : '' }}>1</option>
                                <option {{ $item->qty == 2 ? 'selected' : '' }}>2</option>
                                <option {{ $item->qty == 3 ? 'selected' : '' }}>3</option>
                                <option {{ $item->qty == 4 ? 'selected' : '' }}>4</option>
                                <option {{ $item->qty == 5 ? 'selected' : '' }}>5</option>
                            </select>
                        </td>
                        <td>${{ $item->subtotal }}</td>
                        <td class=""></td>
                        <td>
                            <form action="{{ url('cart', [$item->rowId]) }}" method="POST" class="side-by-side">
                                {!! csrf_field() !!}
                                <inp    ut type="hidden" name="_method" value="DELETE">
                                <input type="submit" class="btn btn-danger btn-sm" value="Remove">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
                        <td>${{ Cart::instance('default')->subtotal() }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Tax</td>
                        <td>${{ Cart::instance('default')->tax() }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="border-bottom">
                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">Your Total</td>
                        <td class="table-bg">${{ Cart::total() }}</td>
                        <td class="column-spacer"></td>
                        <td></td>
                    </tr>
                </tbody>    
            @endif     
          
       <?php else: ?>        
      <?php  $cart_remain[]=DB::table('cart_items')->where('email','=',Auth::user()->email)->get();  ?>      
        
        <?php if(Auth::user()->email==$cart_remain[0][0]->email && sizeof(Cart::content()) > 0){  ?>   
        <?php
        //var_dump('sdjbfdbsfjdfj');exit();
        $count=0;
        if(count($cart_remain[0])>1){
        for($i=0;$i<count($cart_remain[0]);$i++){
         $count+= $cart_remain[0][$i]->qty; 
         $images[]=DB::table('products')->where('id','=',$cart_remain[0][$i]->product_id)->get();  
         //dd($image);
        // dd($images);           
        }
          foreach ($images as $item)            
            $image[] = explode("|", $item[0]->image);
       }     
        ?>   
       
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="table-image"></th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                </thead>
         
                <tbody>
                <?php
                for($i=0;$i<count($cart_remain[0]);$i++) {?>                        
                <tr>              
                     <td><img src="{{ asset('img/' . $image[$i][0]) }}" alt="product" class="img-responsive cart-image" style="width: 150px; height: 150px; "></td>
                    <td>{{$cart_remain[0][$i]->name}}</td>
                    <td>{{$cart_remain[0][$i]->qty}}</td>
                    <td>{{$cart_remain[0][$i]->price}}</td>
                </tr> <?php } ?>
                    <tr>                   
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
                        <td>${{ Cart::instance('default')->subtotal() }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Tax</td>
                        <td>${{ Cart::instance('default')->tax() }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                     <?php exit();?>
                    <tr class="border-bottom">
                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">Your Total</td>
                        <td class="table-bg">${{ Cart::total() }}</td>
                        <td class="column-spacer"></td>
                        <td></td>
                    </tr>
                </tbody> 
                
     <?php  }; ?>
                 </table>


         @if (sizeof(Cart::content()) == 0)
        
        <table class="table">
                <thead>
                    <tr>
                        <th class="table-image"></th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                </thead>
         
                <tbody>
                    @foreach (Cart::content() as $item)
                    <?php
                       $images = array();
                       $images[] = explode("|", $item->model['image']);
                    ?>
                     <tr>
                        <td class="table-image">
                          <a href="{{ url('shop', [$item->model['slug']]) }}">
                            <img src="{{ asset('img/' . $images[0][0]) }}" alt="product" class="img-responsive cart-image">
                          </a>
                        </td>
                        <td><a href="{{ url('shop', [$item->model['slug']]) }}">{{ $item->name }}</a></td>
                        <td>
                            <select class="quantity" data-id="{{ $item->rowId }}">
                                <option {{ $item->qty == 1 ? 'selected' : '' }}>1</option>
                                <option {{ $item->qty == 2 ? 'selected' : '' }}>2</option>
                                <option {{ $item->qty == 3 ? 'selected' : '' }}>3</option>
                                <option {{ $item->qty == 4 ? 'selected' : '' }}>4</option>
                                <option {{ $item->qty == 5 ? 'selected' : '' }}>5</option>
                            </select>
                        </td>
                        <td>${{ $item->subtotal }}</td>
                        <td class=""></td>
                        <td>
                            <form action="{{ url('cart', [$item->rowId]) }}" method="POST" class="side-by-side">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" class="btn btn-danger btn-sm" value="Remove">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
                        <td>${{ Cart::instance('default')->subtotal() }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Tax</td>
                        <td>${{ Cart::instance('default')->tax() }}</td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr class="border-bottom">
                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">Your Total</td>
                        <td class="table-bg">${{ Cart::total() }}</td>
                        <td class="column-spacer"></td>
                        <td></td>
                    </tr>
                </tbody>              
 
        @endif
        @endif          
         
            </table>

            <a href="{{ url('/shop') }}" class="btn btn-primary btn-lg">Continue Shopping</a> &nbsp;


            <form action="{{ url('chk') }}" method="GET" class="side-by-side">
                {!! csrf_field() !!}

                <input type="submit" class="btn btn-success btn-lg" value=" Proceed to Checkout">
            </form>


            <div style="float:right">
                <form action="{{ url('/emptyCart') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger btn-lg" value="Empty Cart">
                </form>
            </div>        

        <div class="spacer"></div>

    </div> <!-- end container -->

@endsection

@section('extra-js')
    <script>
        (function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.quantity').on('change', function() {
                var id = $(this).attr('data-id')
                $.ajax({
                  type: "PATCH",
                  url: '{{ url("/cart") }}' + '/' + id,
                  data: {
                    'quantity': this.value,
                  },
                  success: function(data) {
                    window.location.href = '{{ url('/cart') }}';
                  }
                });

            });

        })();

    </script>
@endsection
