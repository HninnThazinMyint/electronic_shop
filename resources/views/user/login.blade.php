@extends("master")
@section("content")

<?php  if(count($errors)): ?>        <!-- if($errors->has()): -->
<div class="errors">
    <ul>
        <?php foreach ($errors->all() as $err): ?>
            <li><?php echo $err ?></li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>

<?php if(session('info')): ?>
<div class="info">
    <?php echo session('info') ?>
</div>
<?php endif; ?>

<!-- <div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 style="text-align:center;">Login</h3></div>
                <div class="panel-body">
                  <fieldset style="width:400px;padding-left:95px;" class="text-primary">

                  {!! Form::open(['url' => 'user/login', 'method' => 'post']) !!}
                  {{ Form::token() }}

                      <?php  if(count($namex)): ?>
                        {{ Form::hidden('namex', $namex) }}
                      <?php endif; ?>

                    <div class="form-group">
                       {!! Form::label('email', 'Email:') !!}
                       {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Your Email']) !!}
                    </div>

                    <div class="form-group">
                       {!! Form::label('password', 'Password:') !!}
                       {{ Form::password('password', ['class' => 'form-control','placeholder' => 'Enter Password']) }}
                    </div>
                    <div class="form-group">
                      <button class="btn btn-primary">Login</button>&nbsp;&nbsp;
                      <a href="{{url('user/register')}}" >Register</a>
                    </div>
                    {!! Form::close() !!}

                </fieldset>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Sign In</div>
            </div>
            <div style="padding-top:30px" class="panel-body" >
                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                <form method="POST" action="{{url('/user/login')}}" class="form-horizontal" role="form">
                    {!! csrf_field() !!}

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="email" type="text" class="form-control" name="email" value="" placeholder="email">
                    </div>
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input id="password" type="password" class="form-control" name="password" placeholder="password">
                    </div>
                    <div class="input-group">
                        <div class="checkbox">
                            <label>
                                <input id="remember" type="checkbox" name="remember" value="1"> Remember me
                            </label>
                        </div>
                    </div>
                    <div style="margin-top:10px" class="form-group">
                        <div class="col-sm-12 controls">
                            <button type="submit" id="btn-login" href="#" class="btn btn-success">Login  </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                Don't have an account!
                                <a href="{{url('/user/register')}}" >
                                    Sign Up Here
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
