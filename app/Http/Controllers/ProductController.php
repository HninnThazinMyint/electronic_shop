<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $search=$request->get('search');
        $products = Product::where('name','like','%'.$search.'%')
                              ->paginate(12);
        $p=Product::all();

        $category = Category::where('parent_id', '=', 0)->get();

        return view('shop')->with('products', $products)
                            ->with('p', $p)
                           ->with('categories', $category);
    }

    public function index_home()
    {
        $products = Product::all();
        $category = Category::all();
        return view('index')->with('products', $products)
                            ->with('categories', $category);
    }

    public function product_new()
    {
        $category = Category::pluck('name','id')->all();
        return view('products.product-new')->with('categories', $category);
    }

    public function save(Request $request)
{
      $input = $request->all();

      $images=array();
      if($files = $request->file('image'))
      {
        foreach($files as $file)
        {
          $name = $file->getClientOriginalName();
          $file->move('image', $name);
          $images[] = $name;
        }
      }
            //dd($images);
      $v = \Validator::make($request->all(),
          [
              'name'  => 'required',
              'slug' => 'required',
              'description'   => 'required',
              'price'   => 'required',
              'image' => 'required',
              'category_id'=>'required',
          ]);

      if($v->fails())
      {
          return redirect()->back()->withErrors($v->errors());
      }
      else
      {
          $data = array(
              'name'  => $input['name'],
              'slug' => $input['slug'],
              'description' => $input['description'],
              'price'   => $input['price'],
              'category_id'   => $input['category_id'],
              'image'=> implode("|",$images),

              );
          $i =Product::insert($data);
          if($i > 0)
          {
              \Session::flash('message','Record Have Beeen Save Success');

          }
          return  redirect('product-list');
      }
}

public function product_list()
{
    $product = Product::orderBy('id','asc')->paginate(9);

    return view('products.product-list')->with('products', $product);    
}

public function product_del($id)
{
  $i =Product::where('id', $id)->delete();
  if($i>0)
  {
   \Session::flash('message', 'Record have been delete successfully');
   return redirect('product-list');
  }
}

public function product_edit($id)
{
  $product = Product::where('id', $id)->get();
  $allproduct= Product::all();
  $category = Category::pluck('name','id')->all();
  return view('products.product-edit')->with('products', $product)
                              ->with('allproducts', $allproduct)
                              ->with('categories', $category);
}

public function product_update(Request $request)
{
    $input = $request->all();
    //dd($input);
    $images = array();    
    $originalimage=array();
    $product = Product::where('id', $input['id'])->get();   

    if($request->file('imagex')==null){                
        $images[] = explode("|", $product[0]['image']);          
        foreach($images as $image){
              array_push($originalimage,$image);
        }        
    }
    else{

      $images[] = explode("|", $product[0]['image']);          
        foreach($images as $image)
        {
          array_push($originalimage,$image);
        }
      if($files = $request->file('imagex')){
        foreach($files as $file)
        {
        $name = $file->getClientOriginalName();
        $file->move('imagex', $name);
        array_push($originalimage[0], $name);        
        } 
      }  
    } 
    //dd($originalimage);
    $v = \Validator::make($request->all(),
        [
            'name'  => 'required',
            'slug' => 'required',
            'description'   => 'required',
            'price'   => 'required',
            //'image' => 'required',
            'category_id'=>'required',
        ]);
    if($v->fails())
    {
        return redirect()->back()->withErrors($v->errors());
    }
    else
    {
        $data = array(
            'name'  => $input['name'],
            'slug' => $input['slug'],
            'description' => $input['description'],
            'price'   => $input['price'],
            'category_id'   => $input['category_id'],
            'image'=>implode("|", $originalimage[0]),
            );
    }  
      
        Product::where('id', $input['id'])->update($data);
        
        return  redirect('product-list')->withSuccessMessage('Product have been updated!!');
    }
    public function editdel($id,$name){

      $originalimage=array();
      $image=array();
      $product = Product::where('id', $id)->get(); 
      $images[] = explode("|", $product[0]['image']);       
       for($i=0;$i<count($images[0]);$i++)
       {
        if($name!=$images[0][$i]){

          array_push($originalimage,$images[0][$i]);
        }
       }
        //$p=implode("|", $originalimage);
       $data = array(
            'name'  => $product[0]['name'],
            'slug' => $product[0]['slug'],
            'description' => $product[0]['description'],
            'price'   => $product[0]['price'],
            'category_id'   => $product[0]['category_id'],
            'image'=>implode("|", $originalimage),
            );
        Product::where('id', $id)->update($data);
        Pr
        return redirect('product-edit/'.$id);
    }


    public function show($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        $interested = Product::where('slug', '!=', $slug)->get();

        return view('product')->with(['product' => $product, 'interested' => $interested]);
    }

    public function itemlistfunction(Request $request,$id )
    {
      $item = Product::where('category_id', '=', $id)->get();
      //dd($item);

      $items = [];
      foreach ($item as $key => $value) {
        $items[] = array(
          'id' => $value->id,
          'name' => $value->name,
          'slug' => $value->slug,
          'description' => $value->description,
          'price' => $value->price,
          'image' => $value->image,

          'cat_name' => Category::select('name')->where('id', '=', $value->category_id)->first()->name
        );

      }
      $category = Category::where('parent_id', '=', 0)->get();
      //dd($items);
      return view('products.list')->with('products',$items)
                                  ->with('categories', $category);

    }


}
