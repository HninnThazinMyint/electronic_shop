<?php

namespace App;
use App\Order;
use App\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order_items extends Model
{		
	use SoftDeletes;
	public $fillable = ['product_id','order_id','qty'];
	protected $dates = ['deleted_at'];

	 public function order(){
		return $this->belongsTo(Order::class);
			}

		public function product(){
		   return $this->hasOne(Product::class,'id','product_id');
		}

}
