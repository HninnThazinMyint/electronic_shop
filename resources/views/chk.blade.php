@extends('master')

@section('content')

    <div class="container">

        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif

        @if (sizeof(Cart::content()) > 0)

        <div class="row">
          <div class="col-md-6">
            <table class="table" border="2">
                <thead>
                    <tr>
                        <th class="table-image"></th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                </thead>

                <tbody>
                  <form action = "<?php echo URL::to('checkoutregister') ?>" method = "post" >
                    @foreach (Cart::content() as $item)
                    <?php
                       $images = array();
                       $images[] = explode("|", $item->model['image']);

                     ?>
                     <tr>
                       <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
                       <input type="hidden" name="product_id[]" value="{{$item->id}}" >
                       <input type="hidden" name="item_qty[]" value="{{$item->qty}}" >
                       <input type="hidden" name="price[]" value="{{$item->price}}">

                        <td class="table-image" >
                          <a href="{{ url('shop', [$item->model['slug']]) }}">
                            <img src="{{ asset('img/' . $images[0][0]) }}" alt="product" class="img-responsive cart-image">
                          </a>
                        </td>
                        <td><a href="{{ url('shop', [$item->model['slug']]) }}">{{ $item->name }}</a></td>
                        <td>
                            {{$item->qty}}
                        </td>
                        <td>${{ $item->subtotal }}</td>

                    </tr>

                    @endforeach
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
                        <td>${{ Cart::total() }}</td>

                    </tr>
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Shipping</td>
                        <td>$7.00</td>

                    </tr>

                    <tr class="border-bottom">                        
                        <?php
                         $grand =0;
                         foreach(Cart::content() as $item):
                           $grand = $grand+$item->subtotal;
                          $grandtotal=$grand+((13*$grand)/100);
                         endforeach;                       
                      ?>
                     

                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">Your Total</td>
                        <td class="table-bg">${{ $grandtotal+7.00 }}</td>

                    </tr>


                </tbody>
            </table>
          </div>

              <div class="col-md-6">
                  <div class="panel panel-info">
                      <div class="panel-heading">
                          <div class="panel-title">Shipping</div>
                          <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
                      </div>
                      <div class="panel-body" >
                                  {!! csrf_field() !!}
                                  @if(Auth::guest())
                                      <center>
                                        <a href="{{url('/user/orderlogin')}}" class="btn btn-success">
                                        CHECKOUT
                                        </a>
                                      </center>
                              @else
                              @foreach($input as $user)
                              <input type="hidden" name="user_id" value="{{$user->id}}">

                                <div class="form-group">
                                  <label for="email" class="col-md-3 control-label">Email</label>
                                  <div class="col-md-9">
                                      <input type="text" class="form-control" name="email" placeholder="Email Address" value="{{$user->email}}"> <br>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="name" class="col-md-3 control-label">First Name</label>
                                  <div class="col-md-9">
                                      <input type="text" class="form-control" name="name" placeholder="First Name" value="{{$user->name}}"> <br>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="phone" class="col-md-3 control-label">Phone</label>
                                  <div class="col-md-9">
                                      <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{$user->phone}}"> <br>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="address" class="col-md-3 control-label">Address</label>
                                  <div class="col-md-9">
                                      <input type="text" class="form-control" name="address" placeholder="Address" value="{{$user->address}}"> <br>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <!-- Button -->
                                  <div class="col-md-offset-3 col-md-9">
                                      <button type="submit" id="btn-signup" class="btn btn-success"><i class="fa fa-hand-o-right"></i> &nbsp Continue</button>
                                  </div>
                              </div>
                              @endforeach
                              @endif
                          </form>
                      </div>
                  </div>
              </div>

    </div>
</div>

        </div>

            <a href="{{ url('/shop') }}" class="btn btn-primary btn-lg" style="margin-left:40px;">Continue Shopping</a> &nbsp;


        @else

            <h3>You have no items in your shopping cart</h3>
            <a href="{{ url('/shop') }}" class="btn btn-primary btn-lg" >Continue Shopping</a>

        @endif

        <div class="spacer"></div>

    </div> <!-- end container -->

@endsection
