<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;

class CategoryController extends Controller
{

  public function manageCategory()
   {
       $categories = Category::where('parent_id', '=', 0)->get();
       //$allCategories = Category::all();
       $allCategories = Category::pluck('name','id')->all();

       //dd($allCategories);

       return view('categories.categoryTreeview',compact('categories','allCategories'));
   }


   public function addCategory(Request $request)
   {
       $this->validate($request, [
           'name' => 'required',
         ]);
       $input = $request->all();

       $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];

       Category::create($input);
       return back()->with('success', 'New Category added successfully.');
   }

 
  public function cat_del($id)
  {  
    Category::find($id)->deleteCategory();
     return redirect('category-tree-view');
    
  }

  public function cat_edit($id)
  {
    $category = Category::where('id', $id)->get();
    $allcategory = Category::pluck('name','id')->all();
    return view('categories.cat-tree-edit')->with('categories', $category)
                                ->with('allCategories', $allcategory);
  }

  public function cat_update(Request $request)
  {
        $input = $request->all();

         $v = \Validator::make($request->all(),
            [
                'name'  => 'required',
                
            ]);
        if($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }
        else
        {
            $data = array(
                'name'  => $input['name'],
                
                );
            $i = Category::where('id', $input['id'])->update($data);
            if($i > 0)
            {
                \Session::flash('message','Category Have Beeen Update Success');

            }
            return redirect('category-tree-view');
        }

        //dd($post);
        $v = \Validator::make($request->all(),
            [
                'name'  => 'required',
                'parent_id' => 'required',
            ]);
        if($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }
        else
        {
            $data = array(
                'name'  => $input['name'],
                'parent_id' => $input['parent_id'],

                );
            $i = Category::where('id', $input['id'])->update($data);
            if($i > 0)
            {
                \Session::flash('message','Category Have Beeen Update Success');

            }
            return redirect('category-tree-view');
        }
  }


}
