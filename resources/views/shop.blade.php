@extends('master')

@section('content')

    <div class="container">

        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif

  <div class="jumbotron text-center clearfix" >
    <img src="{{ asset('img/L1_TV.jpg') }}" alt="" width="1050">
  </div>  <!--  end jumbotron -->

  <!-- search box -->
    <div class="row" >
            <div class="col-md-5">
                  <form role="search" action="{{url('/search')}}" method="get">
                    <div class="input-group custom-search-form">
                      <input type="text" name="search" class="form-control" placeholder="Search ....">
                      <span class="input-group-btn">&nbsp;
                        <button type="submit" class="btn btn-warning ">
                          <i class="fa fa-search">Search</i>
                        </button>
                      </span>
                    </div>
                  </form>
            </div>
    </div>
    <br><br>
  <!-- end search box -->

    <div class="row" >
        <div class="col-md-12 " >
            <div class="panel panel-success">
                <!-- <div class="panel-heading"></div> -->
                <div class="panel-body">
                    <div class="col-md-3" >
                      <!-- <hr class="color-graph"> -->


                    <div class="panel panel-primary">
                      <div class="panel-heading"><h4>All Categories</h4></div>
                        <div class="panel-body">
                          <ul id="tree1">
                              @foreach($categories as $category)
                                  <li>
                                      <a href="{{ 'shop/category/'.$category->id }}">{{ $category->name }}</a>
                                      
                                      @if(count($res=$category->childs))
                                      <ul>
                                        @foreach($res as $child)
                                        <li>
                                      <a href="{{ 'shop/category/'.$child->id }}">{{ $child->name }}</a>
                                        </li>
                                        @endforeach
                                      </ul>

                                      @endif
                                  </li>
                              @endforeach
                        </div>
                    </div>


                    </div>
                      <div class="col-md-9" >

                                @foreach ($products as $product)
                                  <?php       
                
                                   $images = array();
                                   $images[] = explode("|", $product['image']);
                                 ?>
                                    <div class="col-md-3">
                                        <div class="thumbnail">
                                            <div class="caption text-center">
                                              <?php
                                                 for($i=0; $i<1; $i++){
                                               ?>
                                                <a href="{{ url('shop', [$product->slug]) }}">
                                                  <img src="{{ asset('img/' . $images[0][0]) }}" alt="product" class="img-responsive" style="width: 250px; height: 150px;">
                                                </a>
                                                <?php
                                                    }
                                                 ?>
                                                <a href="{{ url('shop', [$product->slug]) }}"><h5>{{ $product->name }}</h5>
                                                <p>${{ $product->price }}</p>
                                                </a>
                                            </div> <!-- end caption -->
                                            <center>
                                              <form action="{{ url('/cart')}}" method="POST" class="side-by-side">
                                                  {!! csrf_field() !!}
                                                  <input type="hidden" name="id" value="{{ $product->id }}">
                                                  <input type="hidden" name="name" value="{{ $product->name }}">
                                                  <input type="hidden" name="price" value="{{ $product->price }}">
                                                  <input type="submit" class="btn btn-primary btn-sm" value="Add to Cart">
                                              </form>
                                            </center>
                                            
                                        </div> <!-- end thumbnail-->
                                    </div> <!-- end col-md-3 -->
                                @endforeach

                        {{ $products->render() }}
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script src="{{ asset('js/treeview.js') }}"></script> -->

@stop
