<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use Validator;
use Illuminate\Support\Facades\Session;

use Auth;
use App\Models\User;
use App\Models\Customer;
use App\Product;
use DB;
use \Cart as Cart;
use App\Order as Order;
use App\Order_items;
use App\Cart_items;

class UserController extends Controller {

	function index() {
    $namex=Request::get('name');

		if(Auth::check())
			return redirect("profile");	//if return

		return view("user/login",compact('namex'));		//index return
	}

	function login()
	{
		$input = Request::all();
		$cart_remain=Cart_items::where('email','=',$input['email'])->get();
		//return($cart_remain);
		//echo bcrypt($input['password']); exit;
		if(Auth::attempt([
							'email' => $input['email'],
							'password' => $input['password']
						])
		) {
			if(Auth::user()->role == 1){
				return redirect('product-list');
			}else{

					return redirect('/shop/'.str_replace(" ","-",Request::get('namex')));
			}
		}else{
			return redirect('user/login')->withErrors(array('Login failed! Try again.'));	//else reeturn
		}

	}

	function register() {

		return view("user/register");	//register return
	}

	function create() {
		$input = Request::all();

		$validator = Validator::make($input, array(
													"name" => "required",
													"email" => "required|email|unique:users",
													"phone" => "required",
													"address" => "required",
													"gender" => "required",
													"password" => "required|min:6",
													"password_again" => "same:password"
												));

		if($validator->passes()) {

			User::insert($input);
			

			return redirect('user/login')->with('info', 'Register success!');
		} else {
			return redirect('user/register')->withErrors($validator);
		}

	}

	function orderindex() {
  
		return view("user/orderlogin");		//index return
	}

	function orderlogin()
	{
		$input = Request::all();

		if(Auth::attempt([
							'email' => $input['email'],
							'password' => $input['password']
						])
		) {
			
			return redirect('chk');
			
		}
		else{
			return redirect('user/orderlogin')->withErrors(array('Login failed! Try again.'));	//else reeturn
		}

	}

	function orderregister() {

		return view("user/orderregister");	//register return
	}

	function ordercreate() {
		$input = Request::all();

		$validator = Validator::make($input, array(
													"name" => "required",
													"email" => "required|email|unique:users",
													"phone" => "required",
													"address" => "required",
													"gender" => "required",
													"password" => "required|min:6",
													"password_again" => "same:password"
												));

		
		if($validator->fails())
		 {
			return redirect('user/orderregister')->withErrors($validator);
		}
			User::insert($input);

			if (Auth::attempt(['email'=>$input['email'],'password'=>$input['password']])) {

					return redirect('chk');
			
			}
		
			}


	function profile() {
		if(!Auth::check())
			return redirect("user/login");
		$user = Auth::user();
		return view('user/profile', array(
											'id' => $user->id,
											'name' => $user->name,
											'email' => $user->email,
											'address' => $user->address,
											'phone' => $user->phone,
											'gender' => $user->gender,
											'password' => $user->password

										));
	}

	function profile_list()
	{	
		if(!Auth::check())
		return redirect("user/login");
		$user = Auth::user();
		$order=Order::with('orderitem.product')->where('email','=',$user->email)->get();
		return view('user/profile-list', array(
											'id' => $user->id,
											'name' => $user->name,
											'email' => $user->email,
											'address' => $user->address,
											'phone' => $user->phone,
											'gender' => $user->gender,
											'password' => $user->password,
											'order_list'=>$order

										));
	}
	public function detail($id){
		$order=Order::with('orderitem.product')->where('id','=',$id)->get();
		return view('detail',array('order_list'=>$order));
	}


	function update() {
		$input = Request::all();
		//dd($input);
		//$id = Request::find($id);
		User::update($input);
		//DB::table('customer')->where('id', $input['id'])->update($input);

		return redirect("user/profile")->with('info', 'Profile Updated!');

	}

	function logout() {			   
		Cart::destroy();
		Auth::logout();
		return redirect('shop');
	
	}

	function delete($user_id) {
		Auth::logout();
		User::delete($user_id);
		return redirect('user/register');
	}
}
