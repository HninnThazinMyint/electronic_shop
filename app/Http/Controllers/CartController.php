<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \Cart as Cart;
use Validator;
use Auth;
use App\User;
use App\Cart_items;
use \CartItem as CartItem;
class CartController extends Controller
{

    public function index()
    {
        return view('cart');
    }


    public function store(Request $request)
    {        
        if(Auth::user() !=null){
        $user=Auth::user();
        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });

        if (!$duplicates->isEmpty()) {
            return redirect('cart')->withSuccessMessage('Item is already in your cart!');
        }
        Cart::add($request->id, $request->name, 1, $request->price)->associate('App\Product');
       
        for($i=0;$i<1;$i++){
         $data[] = array(
             'email'  => $user['email'],
             'product_id'=>$request['id'],
              'name'=>$request['name'],
              'qty'=>1,
              'price'=>$request['price'],
              );
         } 

        Cart_items::insert($data);
        return redirect('cart')->withSuccessMessage('Item was added to your cart!');
    }

    else{

         $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });

        if (!$duplicates->isEmpty()) {
            return redirect('cart')->withSuccessMessage('Item is already in your cart!');
        }

        Cart::add($request->id, $request->name, 1, $request->price)->associate('App\Product');
      return redirect('cart')->withSuccessMessage('Item was added to your cart!');  
    }
    }


    public function update(Request $request, $id)
    {
        // Validation on max quantity
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,5'
        ]);

         if ($validator->fails()) {
            session()->flash('error_message', 'Quantity must be between 1 and 5.');
            return response()->json(['success' => false]);
         }

        Cart::update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');

        return response()->json(['success' => true]);

    }


    public function destroy($id)
    {
        Cart::remove($id);
        return redirect('cart')->withSuccessMessage('Item has been removed!');
    }


    public function emptyCart()
    {   $email=Auth::user()->email;
        Cart_items::where('email','=',$email)->delete();
        Cart::destroy();
        return redirect('cart')->withSuccessMessage('Your cart has been cleared!');
    }

    public function emptyCartShop()
    {
        Cart::destroy();
        return redirect('shop')->withSuccessMessage('Your cart has been cleared!');
    }

    public function chk()
    {
        if(Auth::guest()){
            return view('chk');
        }
        if(Auth::user()->name){
            $input=User::where('email','=',Auth::user()->email)->get();
            return view('chk')->with('input',$input);
        }
      
    }

    
}
