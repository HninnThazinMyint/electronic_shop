<!-- @extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading text-danger"><h1 class="text-primary">Edit Category</h1></div>
                <div class="panel-body">
                  <form action = "<?= URL::to('cat-update') ?> " method = "post" >
                      <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >

                      @foreach($categories as $category)
                      <input type="hidden" name="id" value="<?php echo $category->id ?>" >
                        <div class="form-group">
                          <label>Category Name</label>
                          <input type="text" name="name" id="name" class="form-control" value="{{$category->name}}">
                        </div>
                        @endforeach
                        <div class="form-group">
                          <label>SubCategory</label>
                          <select name="parent_id" id="parent_id" class="form-control">
                			  	<option value="0">-- Choose --</option>
                			  	<?php
                			    foreach($allcategories as $category){
                			        ?>
                			   	<option value="<?php echo $category->id ?>">
                			   			 <?php echo $category->name ?>
                			   	</option>
                			   	  <?php } ?>
                			   	  </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Update Category" class="btn btn-primary">
                        </div>

              </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
 -->