@extends("master")
@section("content")

<?php if(session('info')): ?>
<div class="info">
	<?php echo session('info') ?>
</div>
<?php endif; ?>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 style="text-align:center;">Customer Profile</h3></div>
                <div class="panel-body">
									<fieldset style="width:600px;padding-left:280px;" class="text-primary">

										{!! Form::open(['url' => 'user/profile', 'method' => 'post']) !!}
	                  {{ Form::token() }}
										{{ Form::hidden('id', $id) }}

										<div class="form-group">
											<label for="name">Name</label>
											<input type="text" name="name" required value="<?php echo $name ?>" class="form-control">
										</div>

										<div class="form-group">
											<label for="email">Email</label>
											<input type="text" name="email" required value="<?php echo $email ?>" class="form-control">
										</div>

										<div class="form-group">
											<label for="phone">Phone</label>
											<input type="text" name="phone" required value="<?php echo $phone ?>" class="form-control">
										</div>

										<div class="form-group">
											<label for="address">Address</label>
											<input type="text" name="address" required value="<?php echo $address ?>" class="form-control">
										</div>

										<div class="form-group">
											<label for="gender">Gender</label>
											<input type="text" name="gender" required value="<?php echo $gender ?>" class="form-control">
										</div>

										<div class="form-group">
											<label for="password">Password</label>
											<input type="password" name="password"  class="form-control">
										</div>

										<input type="submit" value="Update" class="btn btn-success">
										<br><br>

										<a href="<?php echo URL::to("user/logout") ?>" class="text-danger">Logout</a> |
										<a href='<?php echo URL::to("user/delete/$id") ?>' class="text-danger">Delete Account</a>
									</form>
								</fieldset>
								</div>
							</div>
						</div>
					</div>
			<table>
				
			</table>
			</div>
@stop
