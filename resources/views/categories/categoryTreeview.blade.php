@extends("master")
@section("content")

	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">Manage Category</div>
	  		<div class="panel-body">
	  			<div class="row">
	  				<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading"><h4 class="text-primary">Categories List</h4></div>
									<div class="panel-body">
										<ul id="tree1">
												@foreach($categories as $category)
														<li>
																{{ $category->name }}
																[<a href="{{ 'cat-del/'.$category->id}}" >&times;</a>]
																[<a href="{{ 'cat-edit/'.$category->id}}">Edit</a>]

																@if(count($category->childs))
																		@include('categories.manageChild',['childs' => $category->childs])
																@endif

														</li>
														<br>
												@endforeach
										</ul>

									</div>
							</div>
	  				</div>
	  				<div class="col-md-6">
		  					<h3>Add New Category</h3>

								{!! Form::open(['route'=>'add.category']) !!}

				  				@if ($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>
									        <strong>{{ $message }}</strong>
									</div>
								@endif

				  				<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
									{!! Form::label('Name:') !!}
									{!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=>'Enter Name']) !!}
									<span class="text-danger">{{ $errors->first('name') }}</span>
								</div>

								<div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
									{!! Form::label('Parent Category:') !!}
									{!! Form::select('parent_id',$allCategories, old('parent_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
									<span class="text-danger">{{ $errors->first('parent_id') }}</span>
								</div>

								<div class="form-group">
									<button class="btn btn-success">Add New</button>
								</div>

				  			{!! Form::close() !!}

	  				</div>
	  			</div>
	  		</div>
      </div>
    </div>



@stop
