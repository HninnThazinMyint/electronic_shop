@extends('master')

@section('content')

    <div class="container">

        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif

    <div class="jumbotron text-center clearfix" >
      <img src="{{ asset('img/L1_TV.jpg') }}" alt="" width="1050">
    </div>  <!--  end jumbotron -->

    <div class="row" >
        <div class="col-md-12 ">
            <div class="panel panel-success">
                <!-- <div class="panel-heading"></div> -->
                <div class="panel-body" >
                    <div class="col-md-3" >
                        <h3 class="text-primary">All Categories</h3>
                      <!-- <hr class="color-graph"> -->

                      <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                          CATEGORIES
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          @foreach ($categories as $category)
                            <ul>
                              <li><a href="{{ 'indexhome/category/'.$category->id }}">{{$category->name}}</a></li>
                            </ul>
                          @endforeach
                        </ul>
                      </div>

                    </div>
                      <div class="col-md-9" >
                        @foreach ($products->chunk(4) as $items)
                            <div class="row">
                                @foreach ($items as $product)
                                    <div class="col-md-3">
                                        <div class="thumbnail">
                                            <div class="caption text-center">
                                                <a href="{{ url('shop', [$product->slug]) }}"><img src="{{ asset('img/' . $product->image) }}" alt="product" class="img-responsive" style="width: 250px; height: 150px;"></a>
                                                <a href="{{ url('shop', [$product->slug]) }}"><h5>{{ $product->name }}</h5>
                                                <p>${{ $product->price }}</p>
                                                </a>
                                            </div> <!-- end caption -->
                                        </div> <!-- end thumbnail -->
                                    </div> <!-- end col-md-3 -->
                                @endforeach
                            </div> <!-- end row -->
                        @endforeach
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
