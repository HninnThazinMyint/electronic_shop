@extends("master")
@section("content")

	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">Manage Category</div>
	  		<div class="panel-body">
	  			<div class="row">

	  				<div class="col-md-6">
		  					<h3>Update Category</h3>

								@foreach($categories as $category)
								{!! Form::model($category, ['method' => 'PATCH', 'action' => ['CategoryController@cat_update']]) !!}

									{{ Form::token() }}
									{{ Form::hidden('id', $category->id) }}


				  				@if ($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>
									        <strong>{{ $message }}</strong>
									</div>
								@endif

				  				<div class="form-group">
									{!! Form::label('Name:') !!}
									{!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=>'Enter Name']) !!}
								</div>

								<div class="form-group">
									{!! Form::label('Parent Category:') !!}
									@if($category['parent_id']==0)
									{!! Form::select('parent_id',$allCategories,'', ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
									@else
									{!! Form::select('parent_id',$allCategories,old('parent_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}

									@endif
								</div>

								<div class="form-group">
									<button class="btn btn-success">Update</button>
								</div>

				  			{!! Form::close() !!}
								@endforeach

	  				</div>
	  			</div>
	  		</div>
      </div>
    </div>

	<script src="{{ asset('js/treeview.js') }}"></script>

@stop
