<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    public $fillable = ['name','parent_id'];
    protected $dates = ['deleted_at'];

    public function childs() {
        return $this->hasMany('App\Category','parent_id','id') ;
    }
    public function deleteCategory(){
    	$cats=Category::where('parent_id',$this->id);
    	$cats->update(['parent_id'=>$this->parent_id]);
    	$this->delete();
    	return;
    }
}
