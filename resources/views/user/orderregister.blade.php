@extends("master")
@section("content")

<?php if(count($errors)): ?>
<div class="errors">
	<ul>
		<?php foreach($errors->all() as $err): ?>
			<li><?php echo $err ?></li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<div class="container">
    <div style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Sign Up</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
            </div>
            <div class="panel-body" >
                <form action = "<?php echo URL::to('user/orderregister') ?>" method = "post" role="form">
                        {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email" placeholder="Email Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-md-3 control-label">First Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="name" placeholder="First Name">
                        </div>
                    </div>
										<div class="form-group">
												<label for="phone" class="col-md-3 control-label">Phone</label>
												<div class="col-md-9">
														<input type="text" class="form-control" name="phone" placeholder="Phone">
												</div>
										</div>
										<div class="form-group">
												<label for="address" class="col-md-3 control-label">Address</label>
												<div class="col-md-9">
														<input type="text" class="form-control" name="address" placeholder="Address">
												</div>
										</div>
										<div class="form-group">
											<label for="gender" class="col-md-3 control-label">Gender</label>
												<input type="radio" name="gender" value="male">Male
												<input type="radio" name="gender" value="female">Female
										</div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password_again" class="col-md-3 control-label">Confirmation Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="password_again" placeholder="Password">
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" id="btn-signup" class="btn btn-success"><i class="fa fa-hand-o-right"></i> &nbsp Sign Up</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
	</div>

@stop
