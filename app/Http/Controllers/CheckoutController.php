<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Requests;
use \Cart as Cart;
use App\Order as Order;
use App\Order_items;
use App\Customer;
use DB;


class CheckoutController extends Controller
{

    public function index()
    {
        return view('checkout');
    }

    public function save_checkoutregister(Request $request){
        $input = $request->all();
        //dd($input);
        $order_items=array();
        $i=0;
        $v = \Validator::make($request->all(),
          [   

              'name'  => 'required',
              'email' => 'required',
              'phone'   => 'required',
              'address'   => 'required',
          ]);
      if($v->fails())
      {
          return redirect()->back()->withErrors($v->errors());
      }

      else
      {
          $data = array(
              'user_id'=>$input['user_id'],
              'name'  => $input['name'],
              'email' => $input['email'],
              'phone' => $input['phone'],
              'address'=> $input['address'],
              );
             // $id=Customer::insertGetId($data);
             
             $order_id=Order::insertGetId($data);
             while($i<count($input['product_id']))
             {
                $order_items[]=array(
                    'order_id'=>$order_id,
                    'product_id'=>$input['product_id'][$i],
                    'price'=>$input['price'][$i],
                    'qty'=>$input['item_qty'][$i],
                    );
                $i++;
             }
             Order_items::insert($order_items);
             
      }
            return  redirect('submitorder')->withSuccessMessage('Thanks you!!! Your order has been placed!');


    }

    public function order_detail()
    {

      $order_id=array();
      $order_id[]=Order::get();

      $order = Order::with('orderitem.product')->get();
      //return($order);

      return view ('order_detail')->with('order_list',$order);
    }

    public function submitorder()
    {
      return view('submitorder');
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        Cart::instance('checkout')->remove($id);
        return redirect('checkout')->withSuccessMessage('Item has been removed!');
    }


    public function emptyCheckout()
    {
        Cart::instance('checkout')->destroy();
        return redirect('shop');
    }

    public function emptyorder($id)
    {
      Order::where('id','=',$id)->delete();
      Order_items::where('order_id','=',$id)->delete();      

      return redirect('order_detail')->withSuccessMessage('Order have been delivered!!');

    }

}
