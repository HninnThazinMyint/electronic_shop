<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order_items;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{	
	use SoftDeletes;
	public $fillable = ['name','slug','description','price','category_id','image'];
	protected $dates = ['deleted_at'];

	 public function orderitem(){
	     return $this->hasOne(Order_items::class,'product_id');
	}

}
