@extends('master')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 style="text-align: center;">Update Products</h3>
                </div>
                <div class="panel-body">
                  @foreach($products as $product)

                  {!! Form::model($product, ['method' => 'PATCH','files' => true, 'enctype' => 'multipart/form-data', 'action' => ['ProductController@product_update']]) !!}

                    {{ Form::token() }}
                    {{ Form::hidden('id', $product->id) }}

                    <?php
                       $images = array();
                       $images[] = explode("|", $product['image']);
                     ?>

                          <div class="form-group">
                             {!! Form::label('name', 'Name:') !!}
                             {!! Form::text('name', null, ['class' => 'form-control']) !!}
                          </div>

                          <div class="form-group">
                             {!! Form::label('slug', 'Slug:') !!}
                             {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                          </div>

                          <div class="form-group">
                             {!! Form::label('description', 'Description:') !!}
                             {!! Form::textarea('description', null, ['class' => 'form-control','rows' => 5]) !!}
                          </div>

                          <div class="form-group">
                             {!! Form::label('price', 'Price:') !!}
                             {!! Form::text('price', null, ['class' => 'form-control']) !!}
                          </div>

                          <div class="form-group">
                            {!! Form::label('Category:') !!}
                            {!! Form::select('category_id',$categories, old('category_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
                          </div>

                          <div class="form-group">
                            <?php
                               for($i=0; $i<count($images[0]); $i++){
                             ?>
                            <img src="{{asset('img/'.$images[0][$i])}}" class="img-thumbnail" style="width: 60px; height: 60px;">
                            <a href="{{ url('editdel/'.$product->id, $images[0][$i]) }}" onclick="return confirm('Are you sure?');" >&times;</a>
                           
                            <?php } ?>
                            <br>
                            {!! Form::label('Change Image:') !!}

                           {!! Form::file('imagex[]',array('multiple'=>true)) !!}
                          </div>

                          <div class="form-group">
                            <button class="btn btn-success">Update Product</button>
                          </div>

                    {!! Form::close() !!}
                    @endforeach

                  </div>
              </div>
          </div>
      </div>
  </div>
@stop
