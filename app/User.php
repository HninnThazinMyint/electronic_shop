<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'email', 'password','phone','address','gender','role','remember_token',
    ];
    protected $dates = ['deleted_at'];

    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
        {
            return $this->role; // this looks for an admin column in your users table
        }
}
