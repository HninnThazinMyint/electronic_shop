<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>@yield('title', 'Laravel Shopping Cart Example')</title>
    <meta name="description" content="Laravel Shopping Cart Example">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Store CSRF token for AJAX calls -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    @yield('extra-css')

    <!-- Favicon and Apple Icons -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <script src="{{ asset('js/treeview.js') }}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery.js') }}"></script>
    <link href="{{ asset('css/treeview.css') }}" rel="stylesheet">

    <style>

        .spacer {
            margin-bottom: 100px;
        }

        .cart-image {
            width: 100px;
        }

        footer {
            background-color: #f5f5f5;
            padding: 20px 0;
        }

        .table>tbody>tr>td {
            vertical-align: middle;
        }

        .side-by-side {
            display: inline-block;
        }
    </style>
</head>
<body>

    <header>
         <nav class="navbar  navbar-static-top" style="background-color: #eee;">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ url('/') }}">Laravel Electronic Store</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                  @if(Auth::guest())
                  <li class="{{ set_active('shop') }}"><a href="{{ url('/') }}">Home</a></li>
                  @else
                  <li class="{{ set_active('shop') }}"><a href="{{ url('/') }}">Home</a></li>
                  @if(Auth::user()->role == 1)
                    <li><a href="{{ url('/category-tree-view') }}">New Category</a></li>
                    <li><a href="{{ url('/product-new') }}">New Products</a></li>
                    <li><a href="{{ url('/product-list') }}">Product List</a></li>
                    <li><a href="{{ url('/order_detail') }}">Orders Details</a></li>
                  @endif
                @endif
              </ul>
              <ul class="nav navbar-nav navbar-right">
                @if(Auth::guest())
                <li><a href="{{ url('/user/login') }}">Login</a></li>
                <li><a href="{{ url('/user/register') }}">Register</a></li>
                <li class="{{ set_active('cart') }}">
                  <a href="{{ url('/cart') }}" class="btn btn-success">
                    <img src="{{asset('img/cart-icon.jpg')}}" style="width: 30px; height: 30px;">Cart ({{ Cart::instance('default')->count(false) }}) 
                  </a>
                </li>
                @else
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{Auth::user()->name}}<span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/user/logout') }}">Logout</a></li>
                        <li><a href="{{ url('/user/profile-list') }}"></i>Profile</a></li>
                    </ul>
                </li> 
                <?php
                    $count=0; 
                    $cart_remain[]=DB::table('cart_items')->where('email','=',Auth::user()->email)->get();
                    if(count($cart_remain[0])>1){
                    for($i=0;$i<count($cart_remain[0]);$i++){
                     $count+= $cart_remain[0][$i]->qty; 
                    }
                   }        
                ?>             
                @if($count>0)
                @if(Auth::user()->email==$cart_remain[0][0]->email)                
                    <li class="{{ set_active('cart') }}">
                     <a href="{{ url('/cart') }}" class="btn btn-success">
                    <img src="{{asset('img/cart-icon.jpg')}}" style="width: 30px; height: 30px;">Cart ({{ $count }})
                    </a>
                  </li>
                  @endif
                @else
                <!-- <li class="{{ set_active('checkout') }}"><a href="{{ url('/checkout') }}">Checkout ({{ Cart::instance('checkout')->count(false) }})</a></li> -->
                <li class="{{ set_active('cart') }}">
                  <a href="{{ url('/cart') }}" class="btn btn-success">
                    <img src="{{asset('img/cart-icon.jpg')}}" style="width: 60px; height: 30px;">Cart ({{ Cart::instance('default')->count(false) }}) $({{ Cart::total() }})
                  </a>
                </li>
                @endif                
              @endif
                
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>
    </header>

    @yield('content')

    <footer>
      <div class="container">
        <p class="text-muted">By Electronic Shop</a></p>
      </div>
    </footer>

<!-- JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

@yield('extra-js')

</body>
</html>
