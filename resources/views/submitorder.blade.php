@extends('master')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-10">
      <h1 class="text-primary">Thanks you!!! Your order has been placed!</h1><br><br>
      <div style="float:left; margin-left:20%;">
          <form action="{{ url('/emptyCartShop') }}" method="POST">
              {!! csrf_field() !!}
              <input type="hidden" name="_method" value="DELETE">
              <input type="submit" class="btn btn-success btn-lg" value="OK">
          </form>
      </div>
      <br><br><br>
    </div>
  </div>
</div>

@stop
