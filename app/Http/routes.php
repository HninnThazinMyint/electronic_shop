<?php

Route::get('/', function () {
    return redirect('shop');
});

Route::resource('shop', 'ProductController', ['only' => ['index', 'show']]);

Route::get('/shop', 'ProductController@index');

Route::get('/shop/category/{id}', 'ProductController@show');

Route::get('/search', 'ProductController@index');


//for cart & checkout & order

Route::resource('cart', 'CartController');

Route::delete('emptyCart', 'CartController@emptyCart');

Route::post('checkout/{id}', 'CartController@checkout');

Route::get('chk', 'CartController@chk');

Route::delete('emptyCartShop', 'CartController@emptyCartShop');


Route::post('checkoutregister', 'CheckoutController@save_checkoutregister');

Route::get('order','CheckoutController@save_checkoutregister');

Route::get('submitorder','CheckoutController@submitorder');

Route::get('/shop/category/{id}', 'ProductController@itemlistfunction');

Route::get('detail/{id}','UserController@detail');



//for login form

Route::group(['middleware' => ['web']], function () {

		Route::get('/user/login', 'UserController@index');
		Route::post('/user/login', 'UserController@login');

		Route::get('/user/register', 'UserController@register');
		Route::post('/user/register', 'UserController@create');

		Route::get('/user/orderlogin', 'UserController@orderindex');
		Route::post('/user/orderlogin', 'UserController@orderlogin');

		Route::get('/user/orderregister', 'UserController@orderregister');
		Route::post('/user/orderregister', 'UserController@ordercreate');


		Route::get('/user/profile', 'UserController@profile');
		Route::post('/user/profile', 'UserController@update');
		Route::get('/user/profile-list', 'UserController@profile_list');

		Route::get('/user/logout', 'UserController@logout');

		Route::get('/user/delete/{id}', 'UserController@delete');

});

Route::group(['middleware' => ['admin'] ], function () {
		
		//for product
		Route::get('indexhome','ProductController@index_home');

	   	Route::get('product-new','ProductController@product_new');

		Route::post('save','ProductController@save');

		Route::get('product-list','ProductController@product_list');

		Route::get('product-del/{id}','ProductController@product_del');

		Route::get('product-edit/{id}','ProductController@product_edit');

		Route::patch('product-update','ProductController@product_update');

		Route::get('order_detail','CheckoutController@order_detail');

		// for parent category
		Route::get('category-tree-view',['uses'=>'CategoryController@manageCategory']);

		Route::post('add-category',['as'=>'add.category','uses'=>'CategoryController@addCategory']);

		Route::get('cat-edit/{id}','CategoryController@cat_edit');

		Route::patch('cat-update','CategoryController@cat_update');

		Route::get('cat-del/{id}','CategoryController@cat_del');

		Route::get('emptyorder/{id}','CheckoutController@emptyorder');

		Route::get('editdel/{id}/{name}','ProductController@editdel');

 });