@extends("master")
@section("content")

<?php if(session('info')): ?>
<div class="info">
	<?php echo session('info') ?>
</div>
<?php endif; ?>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 style="text-align:center;" >Customer Profile</h3></div>
                <div class="panel-body">
									<fieldset style="width:700px;padding-left:190px;" class="text-primary">

										{!! Form::open(['url' => 'user/profile', 'method' => 'post']) !!}
	                  {{ Form::token() }}
										{{ Form::hidden('id', $id) }}

										<table class="table table-striped table-bordered table-hover" >
										  <tr>
										    <td>Name:</td>
                        <td>{{$name}}</td>
										  </tr>
                      <tr>
                       <td>Email:</td>
                        <td>{{$email}}</td>
                     </tr>
                     <tr>
                       <td>Phone:</td>
                       <td>{{$phone}}</td>
                     </tr>
                     <tr>
                       <td>Address:</td>
                       <td>{{$address}}</td>
                     </tr>
                     <tr>
                       <td>Gender:</td>
                       <td>{{$gender}}</td>
                     </tr>
										</table>


										<a href="<?php echo URL::to("user/profile") ?>" class="text-danger btn btn-success">Update</a> |
										<a href='<?php echo URL::to("user/delete/$id") ?>' class="text-danger btn btn-danger">Delete Account</a>
									</form>
								</fieldset>
								</div>
                    <div class="panel panel-success">
                      <div class="panel-heading">
                        <h4>Order Lists</h4>
                      </div>
                      <div class="panel-body">
                        <table class="table table-striped table-bordered table-bordered table-hover">
                          <tr>
                            <th>Purchase Date</th>
                            <th>Total Amount</th>
                            <th>Equipment</th>
                          </tr>
                          @foreach($order_list as $item)
                          <tr>
                            <td>
                              <b><i>OrderDate::&nbsp;</i></b>{{$item['created_at']}}
                            </td>
                            <td>
                              <?php $total=0;$qty=0; ?>
                            @foreach($item->orderitem as $orderitems)
                              <?php $qty+=$orderitems['qty']; ?>
                              <?php $total+=$orderitems['product']['price']; ?>
                            @endforeach
                            ${{7+($total+((13*$total)/100))}}

                            </td>
                            <td>
                              <a href="{{url('detail/'.$item->id)}}">{{$qty}} items</a>
                            </td>
                          </tr>
                        @endforeach
                        </table>
                      </div>
                    </div>
							</div>
						</div>
					</div>
			</div>
@stop
