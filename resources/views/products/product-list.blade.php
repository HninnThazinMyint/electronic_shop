@extends('master')

@section('content')
<div class="container">
  @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif

  <a href="{{url('product-new')}}" class="btn btn-success pull-right"><img src="img/add.png" alt="">Add Product</a>
<br><br>
 <table class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Id</th>
         <th>Name</th>
         <th>Slug</th>
         <th>Description</th>
         <th>Price</th>
         <th>Images</th>
         <th colspan="3">Action</th>
     </tr>
     </thead>
     <tbody>
     @foreach ($products as $product)
     <?php
        $images = array();
        $images[] = explode("|", $product['image']);
      ?>
         <tr>
             <td>{{ $product->id }}</td>
             <td>{{ $product->name }}</td>
             <td>{{ $product->slug }}</td>
             <td>{{ $product->description }}</td>
             <td>{{ $product->price }}</td>
             <td>
               <?php
                  for($i=0; $i<count($images[0]); $i++){
                ?>
               <img src="{{asset('img/'.$images[0][$i])}}" height="35" width="30">
               <?php
                  }
                ?>
             </td>
             <td>
               <a href="{{ 'product-del/'.$product->id}}" onclick="return confirm('Are you sure?');"
                class="btn btn-danger btn-simple btn-sm">Remove
               </a>
             </td>
             <td>
               <a href="{{ 'product-edit/'.$product->id}}"
                class="btn btn-danger btn-simple btn-sm">Edit
               </a>
             </td>

         </tr>
     @endforeach
     </tbody>
 </table>

 {{ $products->render() }}

 </div>

@stop
