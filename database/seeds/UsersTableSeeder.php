<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'email' => 'member@email.com',
          'password' => Hash::make('password'),
          'name' => 'John Doe',
          'role'=>0
        ]);

        DB::table('users')->insert([
          'email' => 'admin@store.com',
          'password' => Hash::make('adminpassword'),
          'name' => 'Jeniffer Taylor',
          'role'=>1
        ]);

    }
}
