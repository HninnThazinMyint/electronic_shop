@extends('master')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 style="text-align: center;">New Products</h3></div>
                <div class="panel-body">

                    {!! Form::open(['url' => 'save', 'method' => 'post', 'files' => true, 'enctype' => 'multipart/form-data']) !!}

                    <div class="form-group">
                       {!! Form::label('name', 'Name:') !!}
                       {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                       {!! Form::label('slug', 'Slug:') !!}
                       {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                       {!! Form::label('description', 'Description:') !!}
                       {!! Form::textarea('description', null, ['class' => 'form-control','rows' => 5]) !!}
                    </div>

                    <div class="form-group">
                       {!! Form::label('price', 'Price:') !!}
                       {!! Form::text('price', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
    									{!! Form::label('Category:') !!}
    									{!! Form::select('category_id',$categories, old('category_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
    								</div>

                    <div class="form-group">
                      {!! Form::file('image[]',array('multiple'=>true)) !!}
                    </div>

                    <div class="form-group">
                      <button class="btn btn-success">Add Product</button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@stop
