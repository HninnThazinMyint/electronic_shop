<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;
use App\Order_items;

use Illuminate\Database\Eloquent\SoftDeletes;
class Order extends Model
{	
	use SoftDeletes;
	public $fillable = ['user_id','email','name','phone','status'];
	protected $dates = ['deleted_at'];

		public function orderitem(){
		    return $this->hasMany(Order_items::class,'order_id');
		}


}
