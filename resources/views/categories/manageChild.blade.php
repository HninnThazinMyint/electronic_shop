
<ul>
@foreach($childs as $child)
	<li>
		<a href="{{'shop/category/'.$child->id}}">{{ $child->name }}</a>
		[<a href="{{'cat-del/'.$child->id}}" onclick="return confirm('Are you sure?');">&times;</a>]
		[<a href="{{'cat-edit/'.$child->id}}" >Edit</a>]
		@if(count($child->childs))
        @include('categories.manageChild',['childs' => $child->childs])
    @endif
	</li>
@endforeach
</ul>
