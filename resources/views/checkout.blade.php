@extends('master')

@section('content')

    <div class="container">
        <p><a href="{{ url('shop') }}">Home</a> / Checkout</p>
        <h2>Your Checkout</h2>

        <hr>

        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif

        @if (sizeof(Cart::instance('checkout')->content()) > 0)

        <div class="row">
          <div class="col-md-7">
            <table class="table table-striped" style="border: 2px solid #eee;">
                <thead>
                    <tr style="background: #eee;">
                        <th class="table-image">Photos</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                  <form action = "<?php echo URL::to('checkoutregister') ?>" method = "post" >
                    @foreach (Cart::instance('checkout')->content() as $item)
                    <?php
                       $images = array();
                       $images[] = explode("|", $item->model['image']);
                     ?>
                    <tr>
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
                        <input type="hidden" name="product_id[]" value="{{$item->id}}" >
                        <input type="hidden" name="item_qty[]" value="{{$item->qty}}" >

                        <td class="table-image">
                          <a href="{{ url('shop', [$item->model->slug]) }}">
                            <img src="{{ asset('img/' . $images[0][0]) }}" alt="product" class="img-responsive cart-image">
                          </a>
                        </td>
                        <td><a href="{{ url('shop', [$item->model->slug]) }}">{{ $item->name }}</a></td>

                        <td>{{ $item->qty }}</td>
                        <td>${{ $item->total }}</td>
                        <td>
                          <form action="{{ url('checkout', [$item->rowId]) }}" method="POST" class="side-by-side">
                              {!! csrf_field() !!}
                              <input type="hidden" name="_method" value="DELETE">
                              <input type="submit" class="btn btn-danger btn-sm" value="Remove">
                          </form>

                        </td>

                    </tr>
                    @endforeach

                </tbody>

                <tfoot>
                    <tr>

                      <td colspan="2"></td>
                      <td>({{ Cart::instance('checkout')->count(false) }})&nbsp;items</td>
                      <!-- <td colspan="3" style="text-align: right">Total</td> -->
                      <td>${{ Cart::instance('checkout')->total() }}</td>
                    </tr>
                    <tr>
                      <td colspan="3" style="text-align: right">Shipping</td>
                      <td>$7.00</td>
                    </tr>
                    <tr>
                      <?php
                       $grandtotal = 0;
                         foreach(Cart::instance('checkout')->content() as $item):
                           $grandtotal = $grandtotal+$item->total;
                         endforeach;
                      ?>

                      <td colspan="3" style="text-align: right">Total</td>
                      <td>${{$grandtotal+7.00}}</td>
                    </tr>

                </tfoot>
            </table>

          </div>

          <div class="col-md-5">
            <div class="panel panel-primary">
              <div class="panel-heading"><h3>Shipping Information</h3></div>
              <div class="panel-body">
                  <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >

                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" required placeholder="Your Name" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" required placeholder="Your Email" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="phone" name="phone" required placeholder="Your Phone" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="address">Address</label>
                    <textarea name="address" rows="3" cols="20" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <input type="submit" value="Continue" class="btn btn-primary">
                  </div>
              </form>
            </div>
          </div>
      </div>
    </div>
</div>


            <div style="float:left; margin-left:20%;">
                <form action="{{ url('/emptyCheckout') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger btn-lg" value="Empty Checkout">
                </form>
            </div>

        @else

            <h3>You have no items in your Checkout</h3>
            <a href="{{ url('/shop') }}" class="btn btn-primary btn-lg">Continue Shopping</a>

        @endif

        <div class="spacer"></div>

    </div> <!-- end container -->

@endsection
