@extends("master")
@section("content")

<?php if(session('info')): ?>
<div class="info">
	<?php echo session('info') ?>
</div>
<?php endif; ?>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading"><h3 style="text-align:center;" >Order Lists</h3></div>
                <div class="panel-body">
									<fieldset style="width:700px;padding-left:190px;" class="text-primary">

								<div class="panel panel-success">
									<div class="panel-heading">
										<h4>Order Details</h4>
									</div>
									<div class="panel-body" >
										<table class="table table-striped table-bordered table-hover">
											<tr>
												<th>Equipment</th>
                        <th>Price</th>
                        <th>Quantity</th>
											</tr>
                      @foreach ($order_list as $item)
                        @foreach($item->orderitem as $orderitems)
      											<tr>
      													<?php $total = 0; $qty=0;?>

                                  <td>
                                    <a href="{{url('shop/'.$orderitems['product']['slug'])}}">
                                    <?php echo $orderitems['product']['name']; ?>
                                    </a>
                                  </td>

                                  <td>$<?php echo $orderitems['product']['price']; ?></td>

                                  <td><?php     echo $orderitems['qty']; ?></td>

      											</tr>
                        @endforeach
                      @endforeach
                      <tr>
                      <td>Tax</td>
                      <td colspan="2">13%</td>
                      </tr>
                      <tr>
                        <td>Shipping</td>
                        <td colspan="2">$7</td>
                      </tr>

										</table>
									</div>
								</div>
              </fieldset>



							</div>
						</div>
					</div>
			</div>
@stop
